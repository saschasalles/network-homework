# Network Homework 1
First homework of the year.

## I. Gather informations

*  Liste des cartes réseaux, et utilisation du DHCP : `ip a`

```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:1f:ad:fa brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 83358sec preferred_lft 83358sec
    inet6 fe80::1d8f:e581:5569:1297/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:06:32:be brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.101/24 brd 192.168.56.255 scope global dynamic noprefixroute enp0s8
       valid_lft 1155sec preferred_lft 1155sec
    inet6 fe80::ad8b:1c3c:c40b:60a0/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever 
```

 `dynamic` nous dis que nous utilisons le protocol dhcp.

*  Voir les baux (apparement cela se dit comme ça) DHCP :  `/var/lib/NetworkManager/`
 On se met en root pour y acceder.
 On fait un cat sur les interfaces qui nous interresses. Par exemple pour enp0s8 :
 ``` 
 cat  internal-3bf4b7e1-4174-3cf3-b007-b21fca530367-enp0s8.lease
        ADDRESS=192.168.56.101
        NETMASK=255.255.255.0
        SERVER_ADDRESS=192.168.56.100
        T1=600
        T2=1050
        LIFETIME=1200
        CLIENTID=010800270632be
        [root@localhost NetworkManager]# 
```

* Pour afficher la table de routage :

On fait `ip route`, ou sinon on installe et utilise netstat en faisant `netstat  -rn`

Le résultat de `ip route` est :

```
[sascha@localhost /]$ ip route
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100 
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100 
192.168.56.0/24 dev enp0s8 proto kernel scope link src 192.168.56.101 metric 101 

```


* Pour afficher la table ARP : 

`ip neigh show`

explication des lignes ....



Pour afficher la liste des ports en écoute : 

on utilise la commande `ss -ltunp`

```
ss -ltunp
Netid    State      Recv-Q      Send-Q                    Local Address:Port           Peer Address:Port                                                                                                                  
udp      UNCONN     0           0                             127.0.0.1:323                 0.0.0.0:*         users:(("chronyd",pid=750,fd=6))                                                                            
udp      UNCONN     0           0                 192.168.56.101%enp0s8:68                  0.0.0.0:*         users:(("NetworkManager",pid=779,fd=22))                                                                    
udp      UNCONN     0           0                      10.0.2.15%enp0s3:68                  0.0.0.0:*         users:(("NetworkManager",pid=779,fd=18))                                                                    
udp      UNCONN     0           0                                 [::1]:323                    [::]:*         users:(("chronyd",pid=750,fd=7))                                                                            
tcp      LISTEN     0           128                             0.0.0.0:22                  0.0.0.0:*         users:(("sshd",pid=789,fd=6))                                                                               
tcp      LISTEN     0           128                                [::]:22                     [::]:*         users:(("sshd",pid=789,fd=8))

```

On installe `dig` car il n'est pas sur nativement sur CentOS (enfin chez moi en tout cas)
Ducoup `sudo yum install bind-utils`: 

J'éxécute la requête DNS sur reddit.com, avec `dig www.reddit.com +short`

```
[root@localhost /]# dig www.reddit.com +short
reddit.map.fastly.net.
151.101.193.140
151.101.1.140
151.101.65.140
151.101.129.140
```

Et là, nous voyons 4 ip(s), qui correspondent à de la répartition entre les différents serveurs de reddits, pour éviter les surcharges.

* Etat du firewall 

En faisant `firewall-cmd --state`

Nous voyons que le firewall est :
```
[root@localhost /]# firewall-cmd --state
running
```
Ensuite pour savoir quels ports TCP/UDP sont autorisés/filtrés ? 

On lance la commande `firewall-cmd --list-all-zones` (je pense qu'il y a une commande plus appropriée)
Et on s'interresse à la catégorie Work : 

```
work
  target: default
  icmp-block-inversion: no
  interfaces: 
  sources: 
  services: cockpit dhcpv6-client ssh
  ports: 
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
```
Et la nous voyons que sont autorisés `cockpit dhcpv6-client ssh`

* Question bonus 🐙: 
    `nft list table inet firewalld`
    et nous retrouvons les règles en langage nft que nous avions trouvés avec firewall.
    Résultat (qui nous intéresse) : 
    	
```
        chain filter_IN_public_allow {
		tcp dport ssh ct state new,untracked accept
		ip6 daddr fe80::/64 udp dport dhcpv6-client ct state new,untracked accept
		tcp dport 9090 ct state new,untracked accept
        }
```

## II. Edit configuration

**Deuxièmement : Modifier la configuration existante**

Commandes à utiliser (ou pas) : `vim`, `cat`, `nmcli`, `systemctl`, `firewall-cmd`

---

### 1. Configuration cartes réseau


---

* 🌞 modifier la configuration de la carte réseau privée
  * modifier la configuration de la carte réseau privée pour avoir une nouvelle IP statique définie par vos soins
  
```
[root@localhost network-scripts]# cat ifcfg-enp0s8
BOOTPROTO=static
DEVICE=enp0s8
NAME=enp0s8
ONBOOT=yes
IPADDR=192.168.56.105
```  

* ajouter une nouvelle carte réseau dans un DEUXIEME réseau privé UNIQUEMENT privé
  * il faudra par exemple créer un nouveau host-only dans VirtualBox
* 🌞 dans la VM définir une IP statique pour cette nouvelle carte
* vérifier vos changements
  * afficher les nouvelles cartes/IP
```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:1f:ad:fa brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 85515sec preferred_lft 85515sec
    inet6 fe80::1d8f:e581:5569:1297/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:06:32:be brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.105/24 brd 192.168.56.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe06:32be/64 scope link 
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:aa:f4:40 brd ff:ff:ff:ff:ff:ff
    inet 192.168.57.3/24 brd 192.168.57.255 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feaa:f440/64 scope link 
       valid_lft forever preferred_lft forever  
```
 
  * vérifier les nouvelles tables ARP/de routage
  
```  
  [sascha@localhost network-scripts]$ route

Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
default         _gateway        0.0.0.0         UG    100    0        0 enp0s3
10.0.2.0        0.0.0.0         255.255.255.0   U     100    0        0 enp0s3
192.168.56.0    0.0.0.0         255.255.255.0   U     103    0        0 enp0s8
192.168.57.0    0.0.0.0         255.255.255.0   U     104    0        0 enp0s9
```
  
  
* 🐙 mettre en place un NIC *teaming* (ou *bonding*)
  * il vous faut deux cartes dans le même réseau puisque vous allez les agréger (vous pouvez en créer de nouvelles)
  * le *teaming* ou *bonding* consiste à agréger deux cartes réseau pour augmenter les performances/la bande passante
  * je vous laisse free sur la configuration (active/passive, loadbalancing, round-robin, autres)
  * prouver que le NIC *teaming* est en place
 
 Nop désolé, c'est non
 
 ### 2. Serveur SSH

* 🌞 modifier la configuration du système pour que le serveur SSH tourne sur le port 2222

J'ai fait un :
`sudo vi /etc/ssh/sshd_config`
puis ai rajouté le port 2222.

Puis:
```
[root@localhost /]# sudo firewall-cmd --permanent --zone=public --add-port=2222/tcp
success
[root@localhost /]# sudo firewall-cmd --reload
success
```

On ferme le port 22:
`firewall-cmd --zone=public --remove-port=22/tcp`

J'ai fait un reload.
```
[root@localhost ~]# firewall-cmd --list-ports
2222/tcp
```


# III. Routage simple

Pour les VM(s),

**Clients**

|VM `client1.tp1`|VM`client2.tp1`|
|---|---|
|ip `192.168.57.1`| ip `192.168.56.1`| 
|net1 `192.168.57.0/24` | net2 : `192.168.56.0/24`| 

**Routeur**

* Création du routeur `router` dans le réseau `net1` et `net2`. 
Les ip sont les suivantes:
    * `192.168.57.254` pour `net1` 
    * `192.168.56.254` pour `net2`
   
* j'ai lié les ip des VMs à leur nom dans le fichier `/etc/host` :   
    * cat du fichier /etc/hosts du `router`

    ```
    [sascha@router ~]$ cat /etc/hosts
    127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
    ::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
    192.168.57.50 client1.tp1
    192.168.56.50 client2.tp2
    ```

    *  cat du fichier /etc/hosts du du client2

    ```
    [sascha@client2 ~]$ cat /etc/hosts
    127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
    ::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
    192.168.56.254 router.tp1
    192.168.57.50 client1.tp1

    ```

    *  cat du fichier /etc/hosts du client1

    ```
    [sascha@client1 ~]$ cat /etc/hosts
    127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
    ::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
    192.168.56.50 client2.tp1
    192.168.56.254 router.tp1

    ```
* Ping client1 vers client2 :
    ```
    [sascha@client1 ~]$ ping client2.tp1
    PING client2.tp1 (192.168.56.50) 56(84) bytes of data.
    64 bytes from client2.tp1 (192.168.56.50): icmp_seq=1 ttl=63 time=0.321 ms
    64 bytes from client2.tp1 (192.168.56.50): icmp_seq=2 ttl=63 time=1.09 ms
    64 bytes from client2.tp1 (192.168.56.50): icmp_seq=3 ttl=63 time=0.83 ms
    64 bytes from client2.tp1 (192.168.56.50): icmp_seq=4 ttl=63 time=0.27 ms
    ^C
    --- client2.tp1 ping statistics ---
    4 packets transmitted, 4 received, 0% packet loss, time 3ms
    ```

## IV. Autres applications et métrologie

### iftop 
`iftop` permet de voir toutes les connexions d'un ordi et de donner une représentation du débit.











