# TP3 : Routage INTER-VLAN + mise en situation

# Sommaire

* [Intro](#intro)
* [0. Etapes préliminaires](#0-etapes-préliminaires)
* [I. *Router-on-a-stick*](#i-router-on-a-stick)
* [II. Cas concret](#ii-cas-concret)



# 0. Etapes préliminaires

**Dans ce TP, on considère que :**
* les `PC` sont **des VPCS de GNS3** (sauf indication contraire)
* les `P` sont des imprimantes, on les simulera avec des VPCS aussi
* les `SRV` sont ds serveurs, VPCS again
* les `SW` sont des Switches Cisco, virtualisé avec **l'IOU L2**
* les `R` sont des routeurs, virtualisé avec l'iOS dispo ici : **Cisco 3640**

# I. *Router-on-a-stick*

C'est le cas d'école typique pour mettre en place du routage inter-VLAN.  

L'idée est de pouvoir autoriser certains VLANs à se joindre, mais pas d'autres :
* avec les VLANs on isole les gens au niveau 2 (Ethernet)
* avec le routage inter-VLAN, on permet de passer outre les VLANs en faisant appel au niveau 3
* **l'idée c'est qu'à aucun moment on change le fonctionnement des VLANs, on autorise juste un routeur à faire son taff : router entre deux réseaux**, qu'ils correspondent à des VLANs différents ou non
* let's goooo

Schéma moche ftw :

```
             +--+
             |R1|
             +-++
               |
               |                    +---+
               |          +---------+PC4|
+---+        +-+-+      +---+       +---+
|PC1+--------+SW1+------+SW2|
+---+        +-+-+      +-+--+
               |          |  |
               |          |  +------+--+
               |          |         |P1|
             +-+-+      +-+-+       +--+
             |PC2|      |PC3|
             +---+      +---+
```

**Tableau des réseaux utilisés**

Réseau | Adresse | VLAN | Description
--- | --- | --- | ---
`net1` | `10.3.10.0/24` | 10 | Utilisateurs
`net2` | `10.3.20.0/24` | 20 | Admins
`net3` | `10.3.30.0/24` | 30 | Visiteurs
`netP` | `10.3.40.0/24` | 40 | Imprimantes

**Tableau d'adressage**

Machine | VLAN | IP `net1` | IP `net2` | IP `net3` |  IP `netP`
--- | --- | --- | --- | --- | ---
PC1 | 10 | `10.3.10.1/24` | x | x | x
PC2 | 20 | x | `10.3.20.2/24` | x | x | x
PC3 | 20 | x | `10.3.20.3/24` | x | x | x
PC4 | 30 | x | x |  `10.3.30.4/24` | x | x
P1 | 40 | x | x | x | `10.3.40.1/24` 
R1 | x |  `10.3.10.254/24` | `10.3.20.254/24` | `10.3.30.254/24` | `10.3.40.254/24` 

**Qui peut joindre qui ?**

✅ = peuvent se joindre  
 ❌ = ne peuvent pas se joindre

Réseaux | `net1` |  `net2` |  `net3` |  `netP`
--- | --- | --- | --- | ---
 `net1` | ✅ | ❌ | ❌ | ❌
 `net2` | ❌ | ✅ | ✅ | ✅
 `net3` | ❌ | ✅ | ✅ | ✅
 `netP` | ❌ | ✅ | ✅ | ✅

**Instructions** (pretty straightforward) :
* Setup this shit
![setupshit](./screens/gns1.png)

**Mise en place des Vlans sur les switchs, test avec ping.**

* `net1` vers `net1` 
  * Ping `pc1` vers `pc1`

    ```
    PC1> ping 10.3.10.1
    10.3.10.1 icmp_seq=1 ttl=64 time=0.001 ms
    10.3.10.1 icmp_seq=2 ttl=64 time=0.001 ms
    10.3.10.1 icmp_seq=3 ttl=64 time=0.001 ms
    10.3.10.1 icmp_seq=4 ttl=64 time=0.001 ms
    10.3.10.1 icmp_seq=5 ttl=64 time=0.001 ms
    ```
 
* `net1` vers `net2`
  * Ping `pc1` vers `pc2`
    ```
    PC1> ping 10.3.20.2     
    host (255.255.255.0) not reachable
    ```
* `net2` vers `net2`
  * Ping `pc2`vers `pc3`
    ```
    PC2> ping 10.3.20.3
    84 bytes from 10.3.20.3 icmp_seq=1 ttl=64 time=2.428 ms
    84 bytes from 10.3.20.3 icmp_seq=2 ttl=64 time=1.006 ms
    84 bytes from 10.3.20.3 icmp_seq=3 ttl=64 time=1.317 ms
    84 bytes from 10.3.20.3 icmp_seq=4 ttl=64 time=0.375 ms
    84 bytes from 10.3.20.3 icmp_seq=5 ttl=64 time=1.074 ms
    ```

* `net2` vers `net3`
  * Ping `pc3` vers `pc4`
    ```
    PC3> ping 10.3.30.4
    84 bytes from 10.3.30.4 icmp_seq=1 ttl=63 time=1.212 ms
    84 bytes from 10.3.30.4 icmp_seq=2 ttl=63 time=1.416 ms
    84 bytes from 10.3.30.4 icmp_seq=3 ttl=63 time=2.113 ms
    84 bytes from 10.3.30.4 icmp_seq=4 ttl=63 time=1.061 ms
    ^C
    ```

* `net3` vers `netP`
  * Ping `pc4` vers `p1`
    ```
    PC4> ping 10.3.40.1
    84 bytes from 10.3.40.1 icmp_seq=1 ttl=63 time=1.030 ms
    84 bytes from 10.3.40.1 icmp_seq=2 ttl=63 time=1.281 ms
    84 bytes from 10.3.40.1 icmp_seq=3 ttl=63 time=1.043 ms
    84 bytes from 10.3.40.1 icmp_seq=4 ttl=63 time=2.210 ms
    ^C
    ```

* `net3` vers `net1`
  * Ping `pc4` vers `pc1`
    ```
    PC4> ping 10.3.10.1
    10.3.10.1 icmp_seq=1 timeout
    10.3.10.1 icmp_seq=2 timeout
    10.3.10.1 icmp_seq=3 timeout
    ^C
    ```

# II. Cas concret



**Creusez-vous un peu la tête.**  

Le but est de mettre en place une infra qui répond au besoin des bureaux représentés ci-dessous :
* `R1` `R3` `R4` et `R5` sont des bureaux avec des utilisateurs
* `R2` est une salle serveur 
* le bâtiment a une taille de 20m x 20m (approximativement, vous en aurez besoin sur la fin)

**Qui a accès à qui exactement ?** (à mettre en place dans un second temps)  

✅ = peuvent se joindre
❌ = ne peuvent pas se joindre


---

**TODO**

**pour la partie soft**
  * 🌞 dimensionnez intelligemment les réseaux

 Mon réseau compte 38 machines, je prévois environ 30% d'IP supplémentaires, on devrait donc être en mesure de pouvoir fournir envrion 50 ip(s).
Je choisis donc un sous réseau en /26 avec 62 ip(s) à disposition.

* 🌞 permettre un accès internet à tout le monde

J'ajoute un nuage NAT connecté à mon routeur afin de pouvoir fournir un accès internet à tout le monde.

> ping vers google à partir d'une machine d'un stagière.

```
S1> ping 8.8.8.8
84 bytes from 8.8.8.8 icmp_seq=1 ttl=61 time=102.419 ms
84 bytes from 8.8.8.8 icmp_seq=2 ttl=61 time=107.118 ms
84 bytes from 8.8.8.8 icmp_seq=3 ttl=61 time=111.173 ms
84 bytes from 8.8.8.8 icmp_seq=4 ttl=61 time=147.191 ms
84 bytes from 8.8.8.8 icmp_seq=5 ttl=61 time=97.208 ms  
```

* pour la partie hard
  * 🌞 proposez un nombre de routeur et de switches et précisez à quel endroit physique ils se trouveront.

>Je propose un routeur, puis deux switches dont un avec 8 interfaces ethernet (sur GNS3) et un avec 4 interfaces, chaque interface permet l'utilisation de 4 ports. 

>Je fait le choix de mettre les switchs et le routeur dans la même salle 

 J'ai fait le choix de ne prendre que deux switches pour des questions de coût, bien que j'ignore le prix d'un switch à 26 ports, je pense que c'est moins cher qu'un switch par salle.

  * 🌞 précisez le nombre de câbles nécessaires et une longueur (approximative)

38 machines = 38 cables  
On rajoute : 
* 1 cable entre les 2 switches 
* 1 cable entre le routeur et le switch
* 1 cable entre le routeur et le NAT donc

Il faut donc 41 cables au total.

  Il faut environ 
    * court : moins de 1m
    * moyen : entre 1 et 5m
    * long : 5m+
    * **le but c'est d'avoir un ordre de grandeur**, on s'en fout complet des tailles exactes pour ce TP
* 🌞 livrer, en plus de l'infra, des éléments qui rendent compte de l'infra (de façon simple)
  * schéma réseau (screen GNS ?)

  **Schéma lisible** 
  ![setupshit](./screens/infra.png)

  **Schéma représentatif (pour la longueur des cables**
  ![setupshit](./screens/schlisible.png)

  * référez-vous à la partie I. (tableau des réseaux utilisés, tableau d'adressage)


Machine | VLAN | IP `net1` | IP `net2` | IP `net3` |  IP `net4` | IP `net5` | IP `net6` |
| --- | --- | --- | --- | --- | --- | --- | --- | 
Admins | 10 | `10.3.10.1-3` | ... | ... | ... | ... | ... |
Users | 20 | ... | `10.3.20.1-16` | ... | ... | ... | ... |
Stagiaires | 30 | ... | ... | `10.3.30.1-8` | ... | ... | ... |... |
Serveurs | 40 | ... | ... | ... | `10.3.40.1-4` | ... | ... |
Serveurs sensibles | 50 | ... | ... | ... | ... | `10.3.50.1-2`  | ... |
Imprimantes | 60 | ... | ... | ... | ... | ... | `10.3.60.1-5` |

* **être en mesure de prouver que l'infra fonctionne comme demandé**

**Dans un second temps :**
* 🌞 mettre en place "qui a accès à qui exactement ?"

**Conseils**
* **avant de vous lancer** réfléchissez aux différentes étapes qui vous permettront de réaliser le TP
  * je vous conseille par exemple de faire un schéma et un plan d'adressage **en premier**
* documentez ce que vous faites au fur et à mesure
* n'oubliez pas de sauvegarder la configuration des équipements réseau et celle des VPCS

---

**Bonus**
* 🐙 mettre en place les exceptions
  * documentez-vous, proposez des choses
* 🐙 mettre en place un serveur DHCP 
  * il devra 
    * s'intégrer à l'existant
    * être installé sur une VM dédiée (Virtualbox, Workstation)
    * permettre l'attribution d'IPs pour tous les PCs clients (admins, users, stagiaires)
    * libre choix de l'OS (m'enfin, déconnez pas, on va pas mettre un Windows Server 2016 si ?...)
  * mise en place d'un test avec l'ajout d'un nouveau client
