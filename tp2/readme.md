# I. Simplest setup

#### Topologie

```
+-----+        +-------+        +-----+
| PC1 +--------+  SW1  +--------+ PC2 |
+-----+        +-------+        +-----+
```

#### Plan d'adressage

Machine | `net1`
--- | ---
`PC1` | `10.2.1.1/24`
`PC2` | `10.2.1.2/24`

#### ToDo

> **Si vous lancez Wireshark, et que vous mettez des dumps/captures d'écran, précisez où vous avez lancé Wireshark (sur quel lien réseau/quelle machine et quelle interface)**

> **Lancez Wireshark directement depuis l'UI de GNS3** (clic-droit sur un lien > start capture)

* 🌞 mettre en place la topologie ci-dessus
![wireshark 1](screens/1.png/?raw=true "Wireshark 1")
* 🌞 faire communiquer les deux PCs
  * avec un `ping` qui fonctionne
   
 Depuis PC1, ping vers PC2 :
   	
```
	PC1> ping 10.2.1.2
	84 bytes from 10.2.1.2 icmp_seq=1 ttl=64 time=0.497 ms
	84 bytes from 10.2.1.2 icmp_seq=2 ttl=64 time=0.661 ms
	84 bytes from 10.2.1.2 icmp_seq=3 ttl=64 time=0.614 ms	
	84 bytes from 10.2.1.2 icmp_seq=4 ttl=64 time=0.271 ms
	84 bytes from 10.2.1.2 icmp_seq=5 ttl=64 time=0.602 ms
```
	
Depuis PC2, ping vers PC1 :
		
```
	PC2> ping 10.2.1.1
	84 bytes from 10.2.1.1 icmp_seq=1 ttl=64 time=0.478 ms
	84 bytes from 10.2.1.1 icmp_seq=2 ttl=64 time=0.656 ms
	84 bytes from 10.2.1.1 icmp_seq=3 ttl=64 time=0.617 ms
	84 bytes from 10.2.1.1 icmp_seq=4 ttl=64 time=0.617 ms
	84 bytes from 10.2.1.1 icmp_seq=5 ttl=64 time=0.701 ms
```

  * déterminer le protocole utilisé par `ping` à l'aide de Wireshark
  
  **J'ai lancé wireshark depuis le cable reliant PC1 au switch.
  On voit que le protocol utilisé par ping est ARP.**
  
  * analyser les échanges ARP:
    * utiliser Wireshark et mettre en évidence l'échange ARP entre les deux machines (`ARP Request` et `ARP Reply`).  
**Lors du ping de PC1 vers PC2, j'ai analysé les échanges ARPs grâce à wireshark, on y voit bien l'ARP Request et l'ARP Reply**
		![wireshark ARP](screens/2.png/?raw=true "Wireshark ARP2")
		![wireshark ARP](screens/3.png/?raw=true "Wireshark ARP3")
    * corréler avec les tables ARP des différentes machines.  
Sur  `PC1`

```
	PC1> show arp     

00:50:79:66:68:01  10.2.1.2 expires in 106 seconds 
```

Sur `PC2`

```
	PC2> show arp     

	00:50:79:66:68:00  10.2.1.1 expires in 95 seconds 
```

Récapitulatif des étapes quand `PC1` exécute `ping PC2` pour la première fois

**Step 1 : ARP request**
* PC1 fait une requete ARP en broadcast, il demande (à tout le monde qui est connecté sur ses ports, chez nous le switch) au switch qui demandera aux utilisateurs qui détient l'ip "10.1.2.2" et communique par la même occasion son ip.
Le fameux (Broadcast, ARP	64,	Who has 10.2.1.2? Tell 10.2.1.1)
Cette requête est donc de type : « quelle est l’adresse MAC correspondant à l’adresse IP 10.1.2.2 ? Répondez à 10.2.1.1.
On appelle cette étape l'ARP Request. 

**Step 2 :ARP Reply**
* PC2 comprend qu'il a l'ip demandée, le fameux "Hey 10.1.2.1 ?? Mais c'est moi!"
et communique sa mac via Arp.

**Questions**

 * Pourquoi le switch n'a pas besoin d'IP ?  
  Le switch ici ne sert que d'interface, il va transmettre la requete en broadcast, à tous ces ports, en attendant qu'un des périphérique qui lui est connecté réponde.

* Pourquoi les machines ont besoin d'une IP pour pouvoir se `ping` ?  
 Les machines ont besoin d'une ip pour se ping, car sans elle il n'y a pas de destination. 



# II. More switches

#### Topologie

```
                        +-----+
                        | PC2 |
                        +--+--+
                           |
                           |
                       +---+---+
                   +---+  SW2  +----+
                   |   +-------+    |
                   |                |
                   |                |
+-----+        +---+---+        +---+---+        +-----+
| PC1 +--------+  SW1  +--------+  SW3  +--------+ PC3 |
+-----+        +-------+        +-------+        +-----+
```

#### Plan d'adressage

Machine | `net1`
--- | ---
`PC1` | `10.2.2.1/24`
`PC2` | `10.2.2.2/24`
`PC3` | `10.2.2.3/24`

#### ToDo

* 🌞 mettre en place la topologie ci-dessus  
![wireshark sch2](screens/4.png/?raw=true "Wireshark Schema 2")

* 🌞 faire communiquer les trois PCs  
Désolé j'ai pas eu la fois de tout te copier, ducoup j'ai screen 
![wireshark sch2](screens/5.png/?raw=true "Wireshark Schema 2")

* 🌞 analyser la table MAC d'un switch
  * `show mac address-table`
```
Vlan    Mac Address       Type        Ports
----    -----------       --------    -----
   1    0050.7966.6800    DYNAMIC     Et0/0
   1    0050.7966.6801    DYNAMIC     Et0/1
   1    0050.7966.6802    DYNAMIC     Et0/2
   1    aabb.cc00.0210    DYNAMIC     Et0/1
   1    aabb.cc00.0310    DYNAMIC     Et0/2
   1    aabb.cc00.0320    DYNAMIC     Et0/1
```

**Explication** (j'ai choisis le switch 1 IOU1):
* Colonne Vlan : un Vlan est un LAN virtuel. C'est le broadcast domain crée par le switch.   
La doc dit "Displays information for a specific VLAN. The VLAN ID range is from 1 to 4094."
* Colonne Mac Address : Les trois premières adresses mac sont celles des pcs, et les trois autres sont celle des switch, y compris celle du switch en lui même.
* Colonne Type : Il s'agit du type de connection de l'addresse mac, ici c'est dynamique.   
La doc dit : `Displays information about the dynamic MAC address table entries only.`
* Colonne Ports : Affiche les cannaux ethernet de l'interface.


🐙 en lançant Wireshark sur les liens des switches, il y a des trames CDP qui circulent. Quoi qu'est-ce ?  
C.D.P = Cisco Delivery Protocol
Le Cisco Discovery Protocol (CDP) c'est un protocole de découverte de réseau qui permet de trouver d'autres périphériques voisins directement connectés (routeur, switch, pont, etc.).
Ici dans WireShark on voit en analysant le lien qui connecte IOU1 à IOU3, que IOU3 lui envoie via ce protocol, des infos le concernant (nb de ports, version software etc..)

![wireshark sch2](screens/6.png/?raw=true "Info trame")


#### Mise en évidence du [Spanning Tree Protocol](/memo/lexique.md#stp-spanning-tree-protocol)

[STP](/memo/lexique.md#stp-spanning-tree-protocol) a été ici automatiquement configuré par les switches eux-mêmes pour éviter une boucle réseau. 

Dans une configuration pareille, les switches ont élu un chemin de préférence.  
Si on considère les trois liens qui unissent les switches :
* `SW1` <> `SW2`
* `SW2` <> `SW3`
* `SW1` <> `SW3`  

**L'un de ces liens a forcément été désactivé.**

On va regarder comment [STP](/memo/lexique.md#stp-spanning-tree-protocol) a été configuré.

* 🌞 déterminer les informations STP 
  * Dans mon cas le `Root Bridge` est IOU3, il bloque un port sur son system.
  ```
  IOU3#show spanning-tree summary     
  Switch is in rapid-pvst mode
  Root bridge for: none
  Extended system ID                      is enabled
  Portfast Default                        is disabled
  Portfast Edge BPDU Guard Default        is disabled
  Portfast Edge BPDU Filter Default       is disabled
  Loopguard Default                       is disabled
  PVST Simulation Default                 is enabled but inactive in rapid-pvst mode
  Bridge Assurance                        is enabled
  EtherChannel misconfig guard            is enabled
  Configured Pathcost method used is short
  UplinkFast                              is disabled
  BackboneFast                            is disabled

  Name                   Blocking Listening Learning Forwarding STP Active
  ---------------------- -------- --------- -------- ---------- ----------
  VLAN0001                     1         0        0         15         16
  ---------------------- -------- --------- -------- ---------- ----------
  1 vlan                       1         0        0         15         16
  ```

Pour plus de précision sur le port bloqué, j'ai tapé la cmd `show spanning-tree blockedports`
```
Name                 Blocked Interfaces List
-------------------- ------------------------------------
VLAN0001             Et0/2
```

On en déduit que son interface ethernet 0/2 est bloqué.


* 🌞 faire un schéma en représentant les informations STP

`IOU1` | `IOU2` | `IOU3`
--- | --- | ---
`transmetteur` | `transmetteur` | `root bridge`
| --- | --- |`port eth 0/2`  


Le port 0/2 de IOU3 servait à communiquer avec IOU2.
**Si PC3 veux ping PC2, il n'aura pas le choix que de passer par :** 
* `PC3` >> `IOU3` >> `IOU1` >> `IOU2` >> `PC2`  

**Confirmer les informations STP(derien mdr)**

  * effectuer un `ping` d'une machine à une autre
  Test du ping PC3 vers PC2
```
PC3> ping 10.2.1.2
84 bytes from 10.2.1.2 icmp_seq=1 ttl=64 time=0.458 ms
84 bytes from 10.2.1.2 icmp_seq=2 ttl=64 time=1.036 ms
84 bytes from 10.2.1.2 icmp_seq=3 ttl=64 time=0.641 ms
84 bytes from 10.2.1.2 icmp_seq=4 ttl=64 time=0.501 ms
84 bytes from 10.2.1.2 icmp_seq=5 ttl=64 time=0.683 ms
```
Je vois sur WireShark que la trame suit le chemin que indiqué plus haut.

#### Reconfigurer STP

* 🌞 changer la priorité d'un switch qui n'est pas le [*root bridge*](/cours/1.md#overview-of-stp-behaviour)
`IOU1` devient root bridge avec la cmd `conf t` puis `spanning-tree vlan 1 priority 8192` 8192 est possible est un multiple de 4096 donc ca devrait fonctionner.
* 🌞 vérifier les changements
```
IOU3#show spanning-tree

VLAN0001
Spanning tree enabled protocol rstp
Root ID    Priority    8193
            Address     aabb.cc00.0310
            This bridge is the root
            Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec
```
# III. Isolation

Ici on va s'intéresser à l'utilisation de [VLANs](#vlan-virtual-local-area-network). 

## 1. Simple
 
#### Topologie
```
+-----+        +-------+        +-----+
| PC1 +--------+  SW1  +--------+ PC3 |
+-----+      10+-------+20      +-----+
                 20|
                   |
                +--+--+
                | PC2 |
                +-----+
```

#### Plan d'adressage

Machine | IP `net1` | VLAN
--- | --- | --- 
`PC1` | `10.2.3.1/24` | 10
`PC2` | `10.2.3.2/24` | 20
`PC3` | `10.2.3.3/24` | 20

#### ToDo

Création des VLANs : 

* vlan 10 :
```
IOU1#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
IOU1(config)#vlan 10
IOU1(config-vlan)#name pc1
IOU1(config-vlan)#exit
```

* vlan 20 :
```
IOU1#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
IOU1(config)#vlan 20
IOU1(config-vlan)#name pc2/pc3
IOU1(config-vlan)#exit
```
Config du port reliant PC1 au switch (e0/0) qui pour lui attribuer le VLAN 10 :

````
IOU1(config)#interface Ethernet 0/0
IOU1(config-if)#switchport mode access
IOU1(config-if)#switchport access vlan 10
````

J'ai fait de même pour le port e0/1 qui relie PC2 et e0/2 qui relie PC3.

**Faire communiquer les PCs deux à deux :**

* Vérifier que PC2 ne peut joindre que PC3 :

Ping vers `PC3` :
```
  PC2> ping 10.2.2.3
  84 bytes from 10.2.2.3 icmp_seq=1 ttl=64 time=0.421 ms
  84 bytes from 10.2.2.3 icmp_seq=2 ttl=64 time=0.626 ms
  84 bytes from 10.2.2.3 icmp_seq=3 ttl=64 time=0.593 ms
  84 bytes from 10.2.2.3 icmp_seq=4 ttl=64 time=0.581 ms
  84 bytes from 10.2.2.3 icmp_seq=5 ttl=64 time=0.883 ms
````
  
Ping vers `PC1`:
```
  PC2> ping 10.2.2.1
  host (10.2.2.1) not reachable
```

**Vérifier que PC1 ne peut joindre personne alors qu'il est dans le même réseau (sad) :**

Ping vers `PC2` :

```
  PC1> ping 10.2.2.2
  host (10.2.2.2) not reachable
Ping vers PC3 :

  PC1> ping 10.2.2.3
  host (10.2.2.3) not reachable
```

## 2. Avec trunk

#### Topologie

```
+-----+        +-------+        +-------+        +-----+
| PC1 +--------+  SW1  +--------+  SW2  +--------+ PC4 |
+-----+      10+-------+        +-------+20      +-----+
                 20|              10|
                   |                |
                +--+--+          +--+--+
                | PC2 |          | PC3 |
                +-----+          +-----+
```

#### Plan d'adressage

Machine | IP `net1` | IP `net2` | VLAN
--- | --- | --- | ---
`PC1` | `10.2.10.1/24` | X | 10
`PC2` | X | `10.2.20.1/24` | 20
`PC3` | `10.2.10.2/24` | X | 10
`PC4` | X | `10.2.20.2/24` | 20

#### ToDo

* 🌞 faire communiquer les PCs deux à deux
  * vérifier que `PC1` ne peut joindre que `PC3`
```  
PC1> ping 10.2.10.2
84 bytes from 10.2.10.2 icmp_seq=1 ttl=64 time=0.270 ms
84 bytes from 10.2.10.2 icmp_seq=2 ttl=64 time=0.421 ms
^C
PC1> ping 10.2.20.1
host (10.2.20.1) not reachable

PC1> ping 10.2.20.2
host (10.2.20.2) not reachable
``` 

  * vérifier que `PC4` ne peut joindre que `PC2`
```
PC4> ping 10.2.20.1
84 bytes from 10.2.20.1 icmp_seq=1 ttl=64 time=0.217 ms
84 bytes from 10.2.20.1 icmp_seq=2 ttl=64 time=0.122 ms
84 bytes from 10.2.20.1 icmp_seq=3 ttl=64 time=0.429 ms
^C


PC4> ping 10.2.10.1
host (10.2.10.1) not reachable

PC4> ping 10.2.10.2
host (10.2.10.2) not reachable
```

