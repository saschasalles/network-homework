# TP4

## Sujet global : refresh

- Setup la topo dans GNS

![Mise en place topo gns](imgs/topo.png)

## Fichiers de configurations

- Admins

  - [admin 1](./confs/admins/admin1.md)
  - [admin 2](./confs/admins/admin2.md)
  - [admin 3](./confs/admins/admin3.md)

- Guests

  - [guest 1](./confs/guests/guest1.md)
  - [guest 2](./confs/guests/guest2.md)
  - [guest 3](./confs/guests/guest3.md)

- Switchs

  - [client sw-1](./confs/switch/client-sw1.md)
  - [client sw-2](./confs/switch/client-sw2.md)
  - [client sw-3](./confs/switch/client-sw3.md)
  - [infra sw1](./confs/switch/infra-sw1.md)

- Guests
  - [guest 1](./confs/guests/guest1.md)
  - [guest 2](./confs/guests/guest2.md)
  - [guest 3](./confs/guests/guest3.md)
- Router
  - [R1](./confs/router/R1.md)

## Vérifications

- Ping Admin vers Admin
  ```
  [admin1> ping 10.5.10.12
  84 bytes from 10.5.10.12 icmp_seq=1 ttl=64 time=0.762 ms
  84 bytes from 10.5.10.12 icmp_seq=2 ttl=64 time=0.814 ms
  ^C
  [admin1> ping 10.5.10.13
  84 bytes from 10.5.10.13 icmp_seq=1 ttl=64 time=0.982 ms
  84 bytes from 10.5.10.13 icmp_seq=2 ttl=64 time=0.439 ms
  ^C
  ```

* Ping Guest vers Guest

  ```
  [guest1> ping 10.5.20.12
  84 bytes from 10.5.20.12 icmp_seq=1 ttl=64 time=0.722 ms
  84 bytes from 10.5.20.12 icmp_seq=2 ttl=64 time=0.214 ms
  ^C
  [guest1> ping 10.5.20.13
  84 bytes from 10.5.10.13 icmp_seq=1 ttl=64 time=0.482 ms
  84 bytes from 10.5.10.13 icmp_seq=2 ttl=64 time=0.831 ms
  ^C
  [guest1> ping 10.5.20.253
  84 bytes from 10.5.10.253 icmp_seq=1 ttl=64 time=1.232 ms
  84 bytes from 10.5.10.253 icmp_seq=2 ttl=64 time=1.116 ms
  ^C
  ```

* Ping Admin vers Guest

  ```
  [admin1> ping 10.5.20.11
  10.5.20.11 icmp_seq=1 timeout
  10.5.20.11 icmp_seq=2 timeout
  84 bytes from 10.5.20.11 icmp_seq=1 ttl=64 time=15.918 ms
  84 bytes from 10.5.20.11 icmp_seq=2 ttl=64 time=14.427 ms
  ^C
  [guest1> ping 10.5.20.12
  84 bytes from 10.5.10.12 icmp_seq=1 ttl=64 time=16.371 ms
  84 bytes from 10.5.10.12 icmp_seq=2 ttl=64 time=14.819 ms
  ^C
  [guest1> ping 10.5.20.13
  10.5.20.13 icmp_seq=1 timeout
  10.5.20.13 icmp_seq=2 timeout
  84 bytes from 10.5.10.13 icmp_seq=1 ttl=64 time=16.382 ms
  84 bytes from 10.5.10.13 icmp_seq=2 ttl=64 time=18.771 ms
  ^C
  ```

* Ping Vm Web vers Vm Dns

  ```
  [saschasalles@web ~]$ ping 10.5.30.11
  PING 10.5.30.11 (10.5.30.11) 56(84) bytes of data.
  64 bytes from 10.5.30.11: icmp_seq=1 ttl=64 time=2.8 ms
  64 bytes from 10.5.30.11: icmp_seq=2 ttl=64 time=3.4 ms
  64 bytes from 10.5.30.11: icmp_seq=3 ttl=64 time=2.1 ms
  ^C
  ```

* Ping Vm Web vers Wan (google)

  ```
  [saschasalles@web ~]$ ping 8.8.8.8
  PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
  64 bytes from 8.8.8.8: icmp_seq=1 ttl=59 time=32.6 ms
  64 bytes from 8.8.8.8: icmp_seq=2 ttl=59 time=28.8 ms
  64 bytes from 8.8.8.8: icmp_seq=3 ttl=59 time=30.1 ms
  ^C
  ```

* Ping Vm Dns vers Wan (google)
  ```
  [saschasalles@dns ~]$ ping 8.8.8.8
  PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
  64 bytes from 8.8.8.8: icmp_seq=1 ttl=59 time=146 ms
  64 bytes from 8.8.8.8: icmp_seq=2 ttl=59 time=182 ms
  64 bytes from 8.8.8.8: icmp_seq=3 ttl=59 time=128 ms
  ^C
  ```

# Sujet 5 - Anonymat en ligne

- 🌞Lancez Wireshark et observez le trafic émis lors d'un trafic utilisant un proxy HTTP, puis un proxy HTTPS.

![WireSharkProxy](./imgs/WireSharkProxy.png)

- Tor

  - 🌞 Lancez Wireshark et observez le trafic émis lors d'un trafic utilisant le Tor Browser, comparé à une connexion classique.

  Mon Host envoie une requete vers l'adresse du VPN visible sur le cadenas vers du Tor Browser.

  ![tor ip](./imgs/torip.png) 
  
  ![WireShark2](./imgs/wireshark2.png)

- Hidden Service Tor
  Création du .onion :  
   
   ![onion screen](./imgs/onion.png)
